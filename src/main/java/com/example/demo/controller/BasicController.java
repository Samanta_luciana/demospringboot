package com.example.demo.controller;

import com.example.demo.Service.MovieService;
import com.example.demo.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BasicController {

    @Autowired
    private MovieService movieService;

    @RequestMapping("/index")
    String hola(){
        return "index";
    }

    @RequestMapping("/addMovie")
    String adios(){
        return "addMovie";
    }

    @PostMapping("/add")
    String addMovie(@RequestParam String movie_name, Model model){
        Movie m = new Movie();
        m.setMovie_name(movie_name);

        movieService.addMovie(m);
        model.addAttribute("message","La película '" + movie_name + "' ha sido añadida.");

        return "redirect:/addMovie";
    }

    @RequestMapping("/deleteMovie")
    String deleteTemplade() {
        return "deleteMovie";
    }

    @DeleteMapping("/delete")
    String deleteMovie(@RequestParam Long movie_id, Model model ) {
        Movie pelicula = movieService.getMovieById(movie_id);
        if (pelicula == null) {
            model.addAttribute("mensaje", "la pelicula con id " + movie_id + " no existe.");
        } else {
            movieService.deleteMovie(movie_id);
            model.addAttribute("message", "La pelicula con id" + movie_id + "ha sido eliminado");
        }

        return "redirect:/deleteMovie";
    }

    @RequestMapping("/editMovie")
    String updateTemplate(){
        return "editMovie";
    }

    @PutMapping("/update")
    String updateMovie(@RequestParam Long movie_id, @RequestParam String movie_name, Model model){
        Movie pelicula = new Movie();
        pelicula.setMovie_name(movie_name);
        //comprobar si ese id es de una peli
        Movie peli_existe = movieService.getMovieById(movie_id);
        if(peli_existe == null){
            //enviar mensaje que no existe la peli
            model.addAttribute("mensaje", "La peli no existe ");
        } else {
            //comprobar si el movie_name cambia
            if(movie_name.equals(peli_existe.getMovie_name())){
                //mensaje que no cambió el nombre de la peli
                model.addAttribute("mensaje", "No se ha cambiado el nombre de la película ");
            } else {
                //actualizar el nombre de la película
                peli_existe.setMovie_name(movie_name);
                movieService.updateMovie(movie_id, pelicula);
                //añadir un mensaje de éxito
                model.addAttribute("mensaje", "Se actualizó el nombre de la película correctamente");
            }
        }
        return "redirect:/editMovie"; // Va a la vista películas
    }

    /*ejercicio de hoy*/
    @RequestMapping("/list")
   String listMovie(Model model ){
        model.addAttribute("pelis",movieService.getAllMovies());
        return  "listMovies";
    }

}
